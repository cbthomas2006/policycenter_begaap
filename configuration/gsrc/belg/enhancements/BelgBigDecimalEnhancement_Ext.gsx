package belg.enhancements

uses gw.pl.currency.MonetaryAmount
uses gw.util.GosuObjectUtil

/**
 * Created with IntelliJ IDEA.
 * User: cthomas
 * Date: 5/19/16
 * Time: 5:14 PM
 * To change this template use File | Settings | File Templates.
 */
enhancement BelgBigDecimalEnhancement_Ext : java.math.BigDecimal {
  /**
   * returns this BigDecimal amount converted to a CurrencyAmount using JPY
   * <br>null input will return null
   */
  property get toEuros() : MonetaryAmount {
    return GosuObjectUtil.defaultIfNull(new MonetaryAmount(this, Currency.TC_EUR), null) as MonetaryAmount
  }
}
