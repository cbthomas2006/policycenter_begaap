package belg.enhancements

uses java.util.Calendar
uses java.math.BigDecimal
uses java.util.HashMap
uses java.lang.Integer

/**
 * Date utility class with calculation logic for BE GAAP
 */
enhancement BelgDateEnhancement_Ext: java.util.Date {
  /**
   * Calculation to return number of financial months between two dates for proration purposes.
   */
  function getBelgFinancialMonthsBetween(endDate: DateTime): int {
    if (this.compareIgnoreTime(endDate) == 0) {
      return 0
    }
    var yearDiff = endDate.Calendar.CalendarYear - this.Calendar.CalendarYear
    var monthDiff = endDate.MonthOfYear - this.MonthOfYear
    var months = yearDiff * 12 + monthDiff

    return months
  }

  /**
   * Overloaded to allow for consideration of policy start date, which determines how installments are calculated.
   * Depending on the start date, a transaction on any given date could be prorated differently, depending on the
   * length of the installment within which the change is made.
   *
   * For example, suppose we make a policy change on Feb 11.
   * For a policy which starts on Jan 1, this change will fall within the 2nd installment (Feb 1-Feb 28 inclusive),
   * which is 28 days long.
   * But for a policy starting on Jan 15, this change will fall into the 1st installment (Jan 15-Feb 14 inclusive),
   * which is 31 days long.
   */
  function getBelgFinancialMonthsBetween(endDate: DateTime, periodStart: DateTime): BigDecimal {
    // decide which slice we're looking at - before or after the transaction effective date.
    // startDate is to make sure we're referencing the date of the transaction itself
    if (this.compareIgnoreTime(endDate) == 0) {
      return 0
    }

    // The number of the installment(s) in which the transaction effective date falls (e.g., 1st, 2nd, or 3rd installment).
    // Note that depending on the slice dates, the date range could overlap up to three installments, so we must check
    // both start and end dates since there may be multiple rates to combine (e.g., a portion in January would be divided
    // by 31 days, and a portion in April would be divided by 30).
    var startDateInstallmentCount = 0
    var endDateInstallmentCount = 0

    // The length of the installment(s) in which the start and end dates fall. Note that these could be in different
    // installments, so we need to capture two installment lengths in case costs overlap a portion of multiple installments
    var lengthOfStartDateInstallment: int
    var lengthOfEndDateInstallment: int

    // Timestamp of when each installments at either end of transaction begin. These values are used to calculate
    // number of days of transaction's validity period that fall into each.
    var startOfStartDateInstallment: DateTime
    var startOfEndDateInstallment: DateTime

    // Figure out which installment the start date is in, as well as how long that installment is
    startDateInstallmentCount = 0
    if (this.compareIgnoreTime(periodStart) != 0) {
      // if startDate equals periodStart, we are in first installment - leave as zero
      startDateInstallmentCount = periodStart.getBelgFinancialMonthsBetween(this)
      if (this.DayOfMonth <= periodStart.DayOfMonth) {
        startDateInstallmentCount -= 1
      }
      if (startDateInstallmentCount < 0) startDateInstallmentCount = 0
    }
    startOfStartDateInstallment = periodStart.addMonths(startDateInstallmentCount)

    // Now do same for end date's installment
    endDateInstallmentCount = periodStart.getBelgFinancialMonthsBetween(endDate)
    if (endDate.DayOfMonth <= periodStart.DayOfMonth) {
      endDateInstallmentCount -= 1
    }
    if (endDateInstallmentCount < 0) endDateInstallmentCount = 0
    // to account for case where end date is within same month as period start

    startOfEndDateInstallment = periodStart.addMonths(endDateInstallmentCount)

    // Now determine the length of the installments at the front and back end of the transaction.
    // These act as our denominators when prorating if there is less than a whole-month increment
    lengthOfStartDateInstallment = startOfStartDateInstallment.differenceInDays(startOfStartDateInstallment.addMonths(1))
    lengthOfEndDateInstallment = startOfEndDateInstallment.differenceInDays(startOfEndDateInstallment.addMonths(1))

    // Find number of days falling into each installment.
    var numDaysFallingInFirstInstallment: int
    var numDaysFallingInLastInstallment: int

    // If start and end dates are within same installment, just get the difference in days between them.
    if (startDateInstallmentCount == endDateInstallmentCount) {
      numDaysFallingInFirstInstallment = this.differenceInDays(endDate)
      numDaysFallingInLastInstallment = 0
      // set to zero since we've already got the total in the first var
    }
        // Otherwise, calculate the two counts separately, since the installment lengths may be different.
    else {
      numDaysFallingInFirstInstallment = this.differenceInDays(startOfStartDateInstallment.addMonths(1))
      // time from start date to end of applicable installment
      numDaysFallingInLastInstallment = startOfEndDateInstallment.differenceInDays(endDate)
      // time from start of last installment to end date
    }

    // Calculate fractional portion of each installment to be included in proration
    var firstInstallmentFraction = numDaysFallingInFirstInstallment as BigDecimal / lengthOfStartDateInstallment as BigDecimal
    var lastInstallmentFraction = numDaysFallingInLastInstallment as BigDecimal / lengthOfEndDateInstallment as BigDecimal

    // Calculate whole-month difference *between* (exclusive of) start and end dates' installments.
    // For a 12-month policy, this figure will be 10, since the first and last month are not included
    // the following line gets number of whole months between (exclusive of) the end of startDate installment and beginning of endDate installment
    var monthDiff: int
    if (startDateInstallmentCount == endDateInstallmentCount) {
      monthDiff = 0
    } else {
      monthDiff = endDateInstallmentCount - (startDateInstallmentCount + 1)
    }

    // Adjust for case where we are in another calendar month, but still within the same installment (e.g., if start date is Jan 15 and end date is Feb 1, it's 0 whole months)
    var monthDiffWithRemainders = monthDiff + firstInstallmentFraction + lastInstallmentFraction
    return monthDiffWithRemainders
  }

  /**
   * Calculation to return whole number of financial quarters between two dates for proration purposes.
   */
  function getBelgQuartersBetween(endDate: DateTime): BigDecimal {
    if (this.compareIgnoreTime(endDate) == 0) {
      return 0
    }
    var yearDiff = endDate.Calendar.CalendarYear - this.Calendar.CalendarYear
    var quarterDiff = ((endDate.MonthOfYear - this.MonthOfYear) / 3bd).setScale(0, CEILING)
    var quarters = yearDiff * 4 + quarterDiff

    return quarters
  }

  /**
   * Calculation to return whole number of financial half-years between two dates for proration purposes.
   */
  function getBelgHalfYearsBetween(endDate: DateTime): int {
    if (this.compareIgnoreTime(endDate) == 0) {
      return 0
    }
    var yearDiff = endDate.Calendar.CalendarYear - this.Calendar.CalendarYear
    var halfYearDiff = ((endDate.MonthOfYear - this.MonthOfYear) / 6bd).setScale(0, CEILING)
    var halfYears = yearDiff * 2 + halfYearDiff

    return halfYears
  }

  /**
   * Calculation to return whole number of financial years between two dates for proration purposes.
   */
  function getBelgFinancialYearsBetween(endDate: DateTime): int {
    if (this.compareIgnoreTime(endDate) == 0) {
      return 0
    }
    var years = endDate.Calendar.CalendarYear - this.Calendar.CalendarYear

    return years
  }

  // Variation on monthly function above. This returns quarters instead of months, and is used in case of quarterly installments.

  function getBelgQuartersBetween(endDate: DateTime, periodStart: DateTime): BigDecimal {
    final var MONTHS_IN_PRORATION_TERM = 3

    // decide which slice we're looking at - before or after the transaction effective date.
    // startDate is to make sure we're referencing the date of the transaction itself
    if (this.compareIgnoreTime(endDate) == 0) {
      return 0
    }

    // The number of the installment(s) in which the transaction effective date falls (e.g., 1st, 2nd, or 3rd installment).
    // Note that depending on the slice dates, the date range could overlap up to three installments, so we must check
    // both start and end dates since there may be multiple rates to combine (e.g., a portion in January would be divided
    // by 31 days, and a portion in April would be divided by 30).
    var startDateInstallmentCount = 0
    var endDateInstallmentCount = 0

    // The length of the installment(s) in which the start and end dates fall. Note that these could be in different
    // installments, so we need to capture two installment lengths in case costs overlap a portion of multiple installments
    var lengthOfStartDateInstallment: int
    var lengthOfEndDateInstallment: int

    // Timestamp of when each installments at either end of transaction begin. These values are used to calculate
    // number of days of transaction's validity period that fall into each.
    var startOfStartDateInstallment: DateTime
    var startOfEndDateInstallment: DateTime

    // Figure out which installment the start date is in, as well as how long that installment is
    startDateInstallmentCount = 0
    if (this.compareIgnoreTime(periodStart) != 0) {
      // if startDate equals periodStart, we are in first installment - leave as zero
      startDateInstallmentCount = periodStart.getBelgQuartersBetween(this)
      if(this < periodStart.addMonths(startDateInstallmentCount * MONTHS_IN_PRORATION_TERM)) {
        startDateInstallmentCount -= 1
      }
      if (startDateInstallmentCount < 0) startDateInstallmentCount = 0
    }
    startOfStartDateInstallment = periodStart.addMonths(startDateInstallmentCount * MONTHS_IN_PRORATION_TERM)

    // Now do same for end date's installment
    endDateInstallmentCount = periodStart.getBelgQuartersBetween(endDate)
    if(endDate < periodStart.addMonths(endDateInstallmentCount * MONTHS_IN_PRORATION_TERM)) {
      endDateInstallmentCount -= 1
    }
    if (endDateInstallmentCount < 0) endDateInstallmentCount = 0
    // to account for case where end date is within same month as period start

    startOfEndDateInstallment = periodStart.addMonths(endDateInstallmentCount * MONTHS_IN_PRORATION_TERM)

    // Now determine the length of the installments at the front and back end of the transaction.
    // These act as our denominators when prorating if there is less than a whole-month increment
    lengthOfStartDateInstallment = startOfStartDateInstallment.differenceInDays(startOfStartDateInstallment.addMonths(MONTHS_IN_PRORATION_TERM))
    lengthOfEndDateInstallment = startOfEndDateInstallment.differenceInDays(startOfEndDateInstallment.addMonths(MONTHS_IN_PRORATION_TERM))

    // Find number of days falling into each installment.
    var numDaysFallingInFirstInstallment: int
    var numDaysFallingInLastInstallment: int

    // If start and end dates are within same installment, just get the difference in days between them.
    if (startDateInstallmentCount == endDateInstallmentCount) {
      numDaysFallingInFirstInstallment = this.differenceInDays(endDate)
      // set to zero since we've already got the total in the first var
      numDaysFallingInLastInstallment = 0
    }
    // Otherwise, calculate the two counts separately, since the installment lengths may be different.
    else {
      numDaysFallingInFirstInstallment = this.differenceInDays(startOfStartDateInstallment.addMonths(MONTHS_IN_PRORATION_TERM))
      // time from start date to end of applicable installment
      numDaysFallingInLastInstallment = startOfEndDateInstallment.differenceInDays(endDate)
      // time from start of last installment to end date
    }

    // Calculate fractional portion of each installment to be included in proration
    var firstInstallmentFraction = numDaysFallingInFirstInstallment as BigDecimal / lengthOfStartDateInstallment as BigDecimal
    var lastInstallmentFraction = numDaysFallingInLastInstallment as BigDecimal / lengthOfEndDateInstallment as BigDecimal

    // Calculate whole-month difference *between* (exclusive of) start and end dates' installments.
    // For a 12-month policy, this figure will be 10, since the first and last month are not included
    // the following line gets number of whole months between (exclusive of) the end of startDate installment and beginning of endDate installment
    var quarterDiff: int
    if (startDateInstallmentCount == endDateInstallmentCount) {
      quarterDiff = 0
    } else {
      quarterDiff = endDateInstallmentCount - (startDateInstallmentCount + 1)
    }

    // Adjust for case where we are in another calendar month, but still within the same installment (e.g., if start date is Jan 15 and end date is Feb 1, it's 0 whole months)
    var quarterDiffWithRemainders = quarterDiff + firstInstallmentFraction + lastInstallmentFraction
    return quarterDiffWithRemainders
  }

  // Variation on monthly function above. This returns half-years instead of months, and is used in case of quarterly installments.
  function getBelgHalfYearsBetween(endDate: DateTime, periodStart: DateTime): BigDecimal {
    final var MONTHS_IN_PRORATION_TERM = 6

    // decide which slice we're looking at - before or after the transaction effective date.
    // startDate is to make sure we're referencing the date of the transaction itself
    if (this.compareIgnoreTime(endDate) == 0) {
      return 0
    }

    // The number of the installment(s) in which the transaction effective date falls (e.g., 1st, 2nd, or 3rd installment).
    // Note that depending on the slice dates, the date range could overlap up to three installments, so we must check
    // both start and end dates since there may be multiple rates to combine (e.g., a portion in January would be divided
    // by 31 days, and a portion in April would be divided by 30).
    var startDateInstallmentCount = 0
    var endDateInstallmentCount = 0

    // The length of the installment(s) in which the start and end dates fall. Note that these could be in different
    // installments, so we need to capture two installment lengths in case costs overlap a portion of multiple installments
    var lengthOfStartDateInstallment: int
    var lengthOfEndDateInstallment: int

    // Timestamp of when each installments at either end of transaction begin. These values are used to calculate
    // number of days of transaction's validity period that fall into each.
    var startOfStartDateInstallment: DateTime
    var startOfEndDateInstallment: DateTime

    // Figure out which installment the start date is in, as well as how long that installment is
    startDateInstallmentCount = 0
    if (this.compareIgnoreTime(periodStart) != 0) {
      // if startDate equals periodStart, we are in first installment - leave as zero
      startDateInstallmentCount = periodStart.getBelgHalfYearsBetween(this)
      if(this < periodStart.addMonths(startDateInstallmentCount * MONTHS_IN_PRORATION_TERM)) {
        startDateInstallmentCount -= 1
      }
      if (startDateInstallmentCount < 0) startDateInstallmentCount = 0
    }
    startOfStartDateInstallment = periodStart.addMonths(startDateInstallmentCount * MONTHS_IN_PRORATION_TERM)

    // Now do same for end date's installment
    endDateInstallmentCount = periodStart.getBelgHalfYearsBetween(endDate)
    if(endDate < periodStart.addMonths(endDateInstallmentCount * MONTHS_IN_PRORATION_TERM)) {
      endDateInstallmentCount -= 1
    }
    if (endDateInstallmentCount < 0) endDateInstallmentCount = 0
    // to account for case where end date is within same month as period start

    startOfEndDateInstallment = periodStart.addMonths(endDateInstallmentCount * MONTHS_IN_PRORATION_TERM)

    // Now determine the length of the installments at the front and back end of the transaction.
    // These act as our denominators when prorating if there is less than a whole-month increment
    lengthOfStartDateInstallment = startOfStartDateInstallment.differenceInDays(startOfStartDateInstallment.addMonths(MONTHS_IN_PRORATION_TERM))
    lengthOfEndDateInstallment = startOfEndDateInstallment.differenceInDays(startOfEndDateInstallment.addMonths(MONTHS_IN_PRORATION_TERM))

    // Find number of days falling into each installment.
    var numDaysFallingInFirstInstallment: int
    var numDaysFallingInLastInstallment: int

    // If start and end dates are within same installment, just get the difference in days between them.
    if (startDateInstallmentCount == endDateInstallmentCount) {
      numDaysFallingInFirstInstallment = this.differenceInDays(endDate)
      // set to last installment to zero since we've already got the total in the first var
      numDaysFallingInLastInstallment = 0
    }
        // Otherwise, calculate the two counts separately, since the installment lengths may be different.
    else {
      numDaysFallingInFirstInstallment = this.differenceInDays(startOfStartDateInstallment.addMonths(MONTHS_IN_PRORATION_TERM))
      // time from start date to end of applicable installment
      numDaysFallingInLastInstallment = startOfEndDateInstallment.differenceInDays(endDate)
      // time from start of last installment to end date
    }

    // Calculate fractional portion of each installment to be included in proration
    var firstInstallmentFraction = numDaysFallingInFirstInstallment as BigDecimal / lengthOfStartDateInstallment as BigDecimal
    var lastInstallmentFraction = numDaysFallingInLastInstallment as BigDecimal / lengthOfEndDateInstallment as BigDecimal

    // Calculate whole-month difference *between* (exclusive of) start and end dates' installments.
    // For a 12-month policy, this figure will be 10, since the first and last month are not included
    // the following line gets number of whole months between (exclusive of) the end of startDate installment and beginning of endDate installment
    var halfYearDiff: int
    if (startDateInstallmentCount == endDateInstallmentCount) {
      halfYearDiff = 0
    } else {
      halfYearDiff = endDateInstallmentCount - (startDateInstallmentCount + 1)
    }

    // Adjust for case where we are in another calendar month, but still within the same installment (e.g., if start date is Jan 15 and end date is Feb 1, it's 0 whole months)
    var halfYearDiffWithRemainders = halfYearDiff + firstInstallmentFraction + lastInstallmentFraction
    return halfYearDiffWithRemainders
  }

  function getBelgYearsBetween(endDate: DateTime, periodStart: DateTime): BigDecimal {
    final var MONTHS_IN_PRORATION_TERM = 12

    // decide which slice we're looking at - before or after the transaction effective date.
    // startDate is to make sure we're referencing the date of the transaction itself
    if (this.compareIgnoreTime(endDate) == 0) {
      return 0
    }

    // The number of the installment(s) in which the transaction effective date falls (e.g., 1st, 2nd, or 3rd installment).
    // Note that depending on the slice dates, the date range could overlap up to three installments, so we must check
    // both start and end dates since there may be multiple rates to combine (e.g., a portion in January would be divided
    // by 31 days, and a portion in April would be divided by 30).
    var startDateInstallmentCount = 0
    var endDateInstallmentCount = 0

    // The length of the installment(s) in which the start and end dates fall. Note that these could be in different
    // installments, so we need to capture two installment lengths in case costs overlap a portion of multiple installments
    var lengthOfStartDateInstallment: int
    var lengthOfEndDateInstallment: int

    // Timestamp of when each installments at either end of transaction begin. These values are used to calculate
    // number of days of transaction's validity period that fall into each.
    var startOfStartDateInstallment: DateTime
    var startOfEndDateInstallment: DateTime

    // Figure out which installment the start date is in, as well as how long that installment is
    startDateInstallmentCount = 0
    if (this.compareIgnoreTime(periodStart) != 0) {
      // if startDate equals periodStart, we are in first installment - leave as zero
      startDateInstallmentCount = periodStart.getBelgFinancialYearsBetween(this)
      if(this < periodStart.addMonths(startDateInstallmentCount * MONTHS_IN_PRORATION_TERM)) {
        startDateInstallmentCount -= 1
      }
      if (startDateInstallmentCount < 0) startDateInstallmentCount = 0
    }
    startOfStartDateInstallment = periodStart.addMonths(startDateInstallmentCount * MONTHS_IN_PRORATION_TERM)

    // Now do same for end date's installment
    endDateInstallmentCount = periodStart.getBelgFinancialYearsBetween(endDate)
    if(endDate < periodStart.addMonths(endDateInstallmentCount * MONTHS_IN_PRORATION_TERM)) {
      endDateInstallmentCount -= 1
    }
    if (endDateInstallmentCount < 0) endDateInstallmentCount = 0
    // to account for case where end date is within same month as period start

    startOfEndDateInstallment = periodStart.addMonths(endDateInstallmentCount * MONTHS_IN_PRORATION_TERM)

    // Now determine the length of the installments at the front and back end of the transaction.
    // These act as our denominators when prorating if there is less than a whole-month increment
    lengthOfStartDateInstallment = startOfStartDateInstallment.differenceInDays(startOfStartDateInstallment.addMonths(MONTHS_IN_PRORATION_TERM))
    lengthOfEndDateInstallment = startOfEndDateInstallment.differenceInDays(startOfEndDateInstallment.addMonths(MONTHS_IN_PRORATION_TERM))

    // Find number of days falling into each installment.
    var numDaysFallingInFirstInstallment: int
    var numDaysFallingInLastInstallment: int

    // If start and end dates are within same installment, just get the difference in days between them.
    if (startDateInstallmentCount == endDateInstallmentCount) {
      numDaysFallingInFirstInstallment = this.differenceInDays(endDate)
      // set to last installment to zero since we've already got the total in the first var
      numDaysFallingInLastInstallment = 0
    }
        // Otherwise, calculate the two counts separately, since the installment lengths may be different.
    else {
      numDaysFallingInFirstInstallment = this.differenceInDays(startOfStartDateInstallment.addMonths(MONTHS_IN_PRORATION_TERM))
      // time from start date to end of applicable installment
      numDaysFallingInLastInstallment = startOfEndDateInstallment.differenceInDays(endDate)
      // time from start of last installment to end date
    }

    // Calculate fractional portion of each installment to be included in proration
    var firstInstallmentFraction = numDaysFallingInFirstInstallment as BigDecimal / lengthOfStartDateInstallment as BigDecimal
    var lastInstallmentFraction = numDaysFallingInLastInstallment as BigDecimal / lengthOfEndDateInstallment as BigDecimal

    // Calculate whole-month difference *between* (exclusive of) start and end dates' installments.
    // For a 12-month policy, this figure will be 10, since the first and last month are not included
    // the following line gets number of whole months between (exclusive of) the end of startDate installment and beginning of endDate installment
    var yearDiff: int
    if (startDateInstallmentCount == endDateInstallmentCount) {
      yearDiff = 0
    } else {
      yearDiff = endDateInstallmentCount - (startDateInstallmentCount + 1)
    }

    // Adjust for case where we are in another calendar month, but still within the same installment (e.g., if start date is Jan 15 and end date is Feb 1, it's 0 whole months)
    var yearDiffWithRemainders = yearDiff + firstInstallmentFraction + lastInstallmentFraction
    return yearDiffWithRemainders
  }
}