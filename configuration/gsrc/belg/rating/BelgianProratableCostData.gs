package belg.rating

uses gw.api.system.PCLoggerCategory
uses gw.api.util.MonetaryAmounts
uses gw.financials.PolicyPeriodFXRateCache
uses gw.financials.Prorater
uses gw.pl.currency.MonetaryAmount
uses gw.pl.logging.Logger

uses java.math.BigDecimal
uses java.util.Date
uses java.lang.IllegalArgumentException
uses gw.rating.CostDataWithOverrideSupport
uses gw.rating.CostData
uses belg.plugins.BelgianProrationPlugin

abstract class BelgianProratableCostData<C extends Cost, PL extends PolicyLine> extends CostDataWithOverrideSupport<C, PL> {

  // private member variables exposed as Gosu properties
  private var _actualProration : BigDecimal as ActualProration
  private var _premium : MonetaryAmount as Premium
  private var _refundMethod : ProrationMethod as RefundMethod

  protected static var _rfLogger : Logger = PCLoggerCategory.RATEFLOW

  construct(effDate : DateTime, expDate : DateTime, c : Currency, rateCache : PolicyPeriodFXRateCache) {
    super(effDate, expDate, c, rateCache)
    ActualProration = 1.0
  }

  construct(effDate : DateTime, expDate : DateTime) {
    super(effDate, expDate)
    ActualProration = 1.0
  }

  /**
   * Creates a new CostData based on data in entity C, a generic subtype of Cost
   */
  construct(cost : C) {
    super(cost)
    ActualProration = cost.ActualProration
    Premium = cost.Premium
    StandardAmount = cost.StandardAmount
  }

  construct(cost : C, rateCache : PolicyPeriodFXRateCache) {
    super(cost, rateCache)
    ActualProration = cost.ActualProration
    Premium = cost.Premium
    StandardAmount = cost.StandardAmount
  }

  /**
   * Implementing this to persist ActualProration
   */
  override function setSpecificFieldsOnCost(line : PL, cost : C) {
    cost.setFieldValue(line.Subtype.Code, line.FixedId)
    cost.ActualProration = ActualProration.setScale(6, RoundingMode)
    cost.Premium = Premium
    setCostProration(line, cost)
  }

  protected override property get KeyValues() : List<Object> {
    return { }  // Return an empty list
  }

  protected override function populateCostEntity(cost : Cost) {
    super.populateCostEntity(cost)
    (cost as C).ActualProration = ActualProration.setScale(6, RoundingMode)
    (cost as C).Premium = Premium
  }

 /**
  * add handling for unusual amounts--like nonlinear earning--by overriding this method.
  * @param termAmount The calculated amount for a standard term
  * @param periodStartDate The start date of the PolicyPeriod
  * @return The correct amount for the range [EffectiveDate, ExpirationDate}
  */
  override protected function computeExtendedAmount(termAmount : BigDecimal, periodStartDate : DateTime) : BigDecimal {
    return prorateByMonths(termAmount, periodStartDate, false)
  }

  protected function prorateByQuarters(termAmount : BigDecimal, periodStartDate : DateTime) : BigDecimal {
    return prorateByQuarters(termAmount, periodStartDate, false)
  }

  /**
   * @deprecated Use gw.pl.currency.MonetaryAmount instead of BigDecimal
   */
  protected function prorateByMonths(termAmount : BigDecimal, periodStartDate : DateTime, prorateToPeriodEnd : boolean) : BigDecimal {
    return prorateByMonths(termAmount.ofDefaultCurrency(), periodStartDate, prorateToPeriodEnd).Amount
  }

  protected function prorateByQuarters(termAmount : BigDecimal, periodStartDate : DateTime, prorateToPeriodEnd : boolean) : BigDecimal {
    return prorateByQuarters(termAmount.ofDefaultCurrency(), periodStartDate, prorateToPeriodEnd).Amount
  }

  protected function prorateByHalfYears(termAmount : BigDecimal, periodStartDate : DateTime, prorateToPeriodEnd : boolean) : BigDecimal {
    return prorateByHalfYears(termAmount.ofDefaultCurrency(), periodStartDate, prorateToPeriodEnd).Amount
  }

  protected function prorateByYears(termAmount : BigDecimal, periodStartDate : DateTime, prorateToPeriodEnd : boolean) : BigDecimal {
    return prorateByYears(termAmount.ofDefaultCurrency(), periodStartDate, prorateToPeriodEnd).Amount
  }

  protected function prorateByMonths(termAmount : MonetaryAmount, periodStartDate : DateTime, prorateToPeriodEnd : boolean) : MonetaryAmount {
    var p = Prorater.forRounding(RoundingLevel, RoundingMode, this.ProrationMethod) as BelgianProrationPlugin.BelgianProrater
    var endDate : DateTime
    var proratedAmount : MonetaryAmount

    _rfLogger.debug("In computeExtendedAmount, ProrationMethod is: ${ProrationMethod}")
      endDate = p.findEndOfRatedTerm(periodStartDate, NumDaysInRatedTerm)

      _rfLogger.debug("\n\tAbout to call prorateByMonths with values: Period: ${periodStartDate}~${endDate}, Cost: ${EffectiveDate}~${ExpirationDate}, termAmount: ${termAmount}")

      var termAmountBD = termAmount.Amount
      if(prorateToPeriodEnd){
        proratedAmount = p.prorateBelgByMonths(periodStartDate, endDate, EffectiveDate, endDate, termAmount)
      } else {
        proratedAmount = p.prorateBelgByMonths(periodStartDate, endDate, EffectiveDate, ExpirationDate, termAmount)
      }

      _rfLogger.debug("\n\tCall end with proratedAmount=" + proratedAmount)
    ActualProration = p.calculateActualProration(proratedAmount, termAmount)
    _rfLogger.info("proratedAmount is ${proratedAmount}")
    return proratedAmount.setScale(2, HALF_UP)
  }

  protected function prorateByQuarters(termAmount : MonetaryAmount, periodStartDate : DateTime, prorateToPeriodEnd : boolean) : MonetaryAmount {
    var p = Prorater.forRounding(RoundingLevel, RoundingMode, this.ProrationMethod) as BelgianProrationPlugin.BelgianProrater
    var endDate : DateTime
    var proratedAmount : MonetaryAmount

    _rfLogger.debug("In computeExtendedAmount, ProrationMethod is: ${ProrationMethod}")
    endDate = p.findEndOfRatedTerm(periodStartDate, NumDaysInRatedTerm)

    _rfLogger.debug("\n\tAbout to call prorateByQuarters with values: Period: ${periodStartDate}~${endDate}, Cost: ${EffectiveDate}~${ExpirationDate}, termAmount: ${termAmount}")

    var termAmountBD = termAmount.Amount
    if(prorateToPeriodEnd){
      proratedAmount = p.prorateBelgByQuarters(periodStartDate, endDate, EffectiveDate, endDate, termAmount)
    } else {
      proratedAmount = p.prorateBelgByQuarters(periodStartDate, endDate, EffectiveDate, ExpirationDate, termAmount)
    }

    _rfLogger.debug("\n\tCall end with proratedAmount=" + proratedAmount)
    ActualProration = p.calculateActualProration(proratedAmount, termAmount)
    _rfLogger.info("proratedAmount is ${proratedAmount}")
    return proratedAmount.setScale(2, HALF_UP)
  }

  protected function prorateByHalfYears(termAmount : MonetaryAmount, periodStartDate : DateTime, prorateToPeriodEnd : boolean) : MonetaryAmount {
    var p = Prorater.forRounding(RoundingLevel, RoundingMode, this.ProrationMethod) as BelgianProrationPlugin.BelgianProrater
    var endDate : DateTime
    var proratedAmount : MonetaryAmount

    _rfLogger.debug("In computeExtendedAmount, ProrationMethod is: ${ProrationMethod}")
    endDate = p.findEndOfRatedTerm(periodStartDate, NumDaysInRatedTerm)

    _rfLogger.debug("\n\tAbout to call prorateByQuarters with values: Period: ${periodStartDate}~${endDate}, Cost: ${EffectiveDate}~${ExpirationDate}, termAmount: ${termAmount}")

    var termAmountBD = termAmount.Amount
    if(prorateToPeriodEnd){
      proratedAmount = p.prorateBelgByHalfYears(periodStartDate, endDate, EffectiveDate, endDate, termAmount)
    } else {
      proratedAmount = p.prorateBelgByHalfYears(periodStartDate, endDate, EffectiveDate, ExpirationDate, termAmount)
    }

    _rfLogger.debug("\n\tCall end with proratedAmount=" + proratedAmount)
    ActualProration = p.calculateActualProration(proratedAmount, termAmount)
    _rfLogger.info("proratedAmount is ${proratedAmount}")
    return proratedAmount.setScale(2, HALF_UP)
  }

  protected function prorateByYears(termAmount : MonetaryAmount, periodStartDate : DateTime, prorateToPeriodEnd : boolean) : MonetaryAmount {
    var p = Prorater.forRounding(RoundingLevel, RoundingMode, this.ProrationMethod) as BelgianProrationPlugin.BelgianProrater
    var endDate : DateTime
    var proratedAmount : MonetaryAmount

    _rfLogger.debug("In computeExtendedAmount, ProrationMethod is: ${ProrationMethod}")
    endDate = p.findEndOfRatedTerm(periodStartDate, NumDaysInRatedTerm)

    _rfLogger.debug("\n\tAbout to call prorateByQuarters with values: Period: ${periodStartDate}~${endDate}, Cost: ${EffectiveDate}~${ExpirationDate}, termAmount: ${termAmount}")

    var termAmountBD = termAmount.Amount
    if(prorateToPeriodEnd){
      proratedAmount = p.prorateBelgByYears(periodStartDate, endDate, EffectiveDate, endDate, termAmount)
    } else {
      proratedAmount = p.prorateBelgByYears(periodStartDate, endDate, EffectiveDate, ExpirationDate, termAmount)
    }

    _rfLogger.debug("\n\tCall end with proratedAmount=" + proratedAmount)
    ActualProration = p.calculateActualProration(proratedAmount, termAmount)
    _rfLogger.info("proratedAmount is ${proratedAmount}")
    return proratedAmount.setScale(2, HALF_UP)
  }

  protected function getProrationFactor(periodStart : Date, periodEnd : Date, rateStart : Date, rateEnd : Date, prorationMethod : ProrationMethod) : BigDecimal{
    var p = Prorater.forRounding(RoundingLevel, RoundingMode, prorationMethod)
    if(prorationMethod == typekey.ProrationMethod.TC_PRORATABYMONTHS) {
      return (p as BelgianProrationPlugin.BelgianProrater).prorateBelgByMonths(periodStart, periodEnd, rateStart, rateEnd, 1bd)
    } else if(prorationMethod == typekey.ProrationMethod.TC_PRORATABYQUARTER) {
      return (p as BelgianProrationPlugin.BelgianProrater).prorateBelgByQuarters(periodStart, periodEnd, rateStart, rateEnd, 1bd)
    } else if(prorationMethod == typekey.ProrationMethod.TC_PRORATABYHALFYEAR) {
      return (p as BelgianProrationPlugin.BelgianProrater).prorateBelgByHalfYears(periodStart, periodEnd, rateStart, rateEnd, 1bd)
    } else if (prorationMethod == typekey.ProrationMethod.TC_PRORATABYDAYS) {
      var totalDays = p.financialDaysBetween(periodStart, periodEnd) as BigDecimal
      if (totalDays == 0) {
        return 0bd
      }
      var daysToProrate = p.financialDaysBetween(rateStart, rateEnd) as BigDecimal
      return daysToProrate / totalDays
    } else if (prorationMethod == typekey.ProrationMethod.TC_FLAT) {
      return 1bd
    } else {
      throw new IllegalArgumentException("ProrationMethod: ${ProrationMethod} is not implemented")
    }
  }

  /**
   * Return proration factor using this.RefundMethod
   */
  protected function getRefundFactor(periodStart : Date, periodEnd : Date, rateStart : Date, rateEnd : Date) : BigDecimal{
    return getProrationFactor(periodStart, periodEnd, rateStart, rateEnd, this.RefundMethod)
  }

  /**
   * Return proration factor using this.ProrationMethod
   */
  protected function getProrationFactor(periodStart : Date, periodEnd : Date, rateStart : Date, rateEnd : Date) : BigDecimal{
    return getProrationFactor(periodStart, periodEnd, rateStart, rateEnd, this.ProrationMethod)
  }

  override function mergeIfCostEqual(other : CostData) : boolean {
    var result = super.mergeIfCostEqual(other)
    if(result){
      EffectiveDate = this.EffectiveDate
      ExpirationDate = (other as BelgianProratableCostData).ExpirationDate
    }
    return result
  }

  override function debugString() : String {
    var output = new java.lang.StringBuffer()
    var tab = '\t'
    output.append("PM: ").append(ProrationMethod.Code).append(tab)
          .append("RateEffDate: ").append(EffectiveDate).append(tab)
          .append("RateExpDate: ").append(ExpirationDate).append(tab)
          .append("AP: ").append(ActualProration).append(tab)
          .append("${this.ChargePattern.Code}").append(tab)
          .append("Eff: ${EffectiveDate}~${ExpirationDate}").append(tab)
          .append("ActualBase: ${ActualBaseRate}").append(tab)
          .append("ActualTerm: ${ActualTermAmount}").append(tab)
          .append(this.Key)
    //intentionally _not_ appending super.debugString() as it's a bit too much info. for monthly proration
    //return output.append(super.debugString()).append(Key).toString()
    return output.toString()
  }


  override protected function computeAmount(termAmount : BigDecimal, periodStartDate : Date) : BigDecimal {
    switch (ProrationMethod) {
      case TC_FLAT: // flat rate -- amount is always termAmount
          return termAmount
      case TC_PRORATABYDAYS: // pro rata based on number of days
          var p = Prorater.forRounding(RoundingLevel, RoundingMode, TC_PRORATABYDAYS)
          var endDate = p.findEndOfRatedTerm(periodStartDate, NumDaysInRatedTerm)
          var proratedAmount = p.prorate(periodStartDate,  endDate, EffectiveDate, ExpirationDate, termAmount)
          return p.scaleAmount(proratedAmount)
      case TC_PRORATABYMONTHS:
          return prorateByMonths(termAmount, periodStartDate, false)
      case TC_PRORATABYQUARTER:
        return prorateByQuarters(termAmount, periodStartDate, false)
      case TC_PRORATABYHALFYEAR:
          return prorateByHalfYears(termAmount, periodStartDate, false)
      case TC_PRORATABYYEAR:
          return prorateByYears(termAmount, periodStartDate, false)
      // If not yet set, assume monthly. It actually doesn't matter for new submissions since they are in whole-month increments, but we must choose a proration method nonetheless.
      case null:
          return prorateByYears(termAmount, periodStartDate, false)
      default:
        throw new IllegalArgumentException("ProrationMethod: ${ProrationMethod} is not implemented")
    }
  }

  function setCostProration(line : PolicyLine, cost : C) {
    if(line.AssociatedPolicyPeriod.SelectedPaymentPlan.InvoiceFrequency == typekey.BillingPeriodicity.TC_MONTHLY) {
      cost.ProrationMethod = typekey.ProrationMethod.TC_PRORATABYMONTHS
    } else if(line.AssociatedPolicyPeriod.SelectedPaymentPlan.InvoiceFrequency == typekey.BillingPeriodicity.TC_QUARTERLY) {
      cost.ProrationMethod = typekey.ProrationMethod.TC_PRORATABYQUARTER
    }
  }

  override function setProrationByInstallmentFreq(installmentFreq : typekey.BillingPeriodicity) {
    if(installmentFreq == BillingPeriodicity.TC_MONTHLY) {
      ProrationMethod = typekey.ProrationMethod.TC_PRORATABYMONTHS
      _refundMethod = typekey.ProrationMethod.TC_PRORATABYMONTHS
    } else if(installmentFreq == BillingPeriodicity.TC_QUARTERLY) {
      ProrationMethod = typekey.ProrationMethod.TC_PRORATABYQUARTER
      _refundMethod = typekey.ProrationMethod.TC_PRORATABYQUARTER
    }
  }
}
