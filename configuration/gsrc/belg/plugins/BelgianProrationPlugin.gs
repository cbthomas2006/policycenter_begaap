package belg.plugins

uses gw.api.system.PCLoggerCategory
uses gw.financials.Prorater
uses gw.pl.currency.MonetaryAmount
uses gw.plugin.policyperiod.IProrationPlugin
uses java.math.RoundingMode

uses java.lang.IllegalArgumentException
uses java.lang.UnsupportedOperationException
uses java.math.BigDecimal
uses java.util.Date
uses gw.plugin.policyperiod.impl.ProrationPlugin

class BelgianProrationPlugin extends ProrationPlugin implements IProrationPlugin {

  override function getProraterForRounding(roundingLevel : int, roundingMode : RoundingMode, prorationMethod : ProrationMethod) : Prorater {
    if (roundingLevel == null) throw new IllegalArgumentException("roundingLevel may not be null")
    if (roundingMode == null) throw new IllegalArgumentException("roundingMode may not be null")
    if (prorationMethod == null) throw new IllegalArgumentException("prorationMethod may not be null")

    if (prorationMethod != typekey.ProrationMethod.TC_FLAT) {
      return new BelgianProrater(roundingLevel, roundingMode, prorationMethod)
    } else {
      return super.getProraterForRounding(roundingLevel, roundingMode, prorationMethod)
    }
  }

  static class BelgianProrater extends Prorater {

    static var _rfLogger = PCLoggerCategory.RATEFLOW
    private var _prorationMethod : ProrationMethod

    override function toString() : String { return "BelgianProrater from BelgianProrationPlugin" }

    construct(level : int, mode : RoundingMode, prorationMethod : ProrationMethod) {
      super(level, mode)
      _prorationMethod = prorationMethod
    }

    override function scaleAmount(amount : BigDecimal) : BigDecimal {
      return amount.setScale(RoundingLevel, RoundingMode)
    }

    override function scaleAmount(amount : MonetaryAmount) : MonetaryAmount {
      return amount.setScale(RoundingLevel, RoundingMode)
    }

    function financialMonthsBetween(startDate : DateTime, endDate : DateTime) : BigDecimal {
      return startDate.getBelgFinancialMonthsBetween(endDate)
    }

    function financialMonthsBetween(startDate : DateTime, endDate : DateTime, periodStart : DateTime) : BigDecimal {
      return startDate.getBelgFinancialMonthsBetween(endDate, periodStart)
    }

    function financialQuartersBetween(startDate : DateTime, endDate : DateTime) : BigDecimal {
      return startDate.getBelgQuartersBetween(endDate)
    }

    function financialQuartersBetween(startDate : DateTime, endDate : DateTime, periodStart : DateTime) : BigDecimal {
      return startDate.getBelgQuartersBetween(endDate, periodStart)
    }

    function financialHalfYearsBetween(startDate : DateTime, endDate : DateTime, periodStart : DateTime) : BigDecimal {
      return startDate.getBelgHalfYearsBetween(endDate, periodStart)
    }

    function financialYearsBetween(startDate : DateTime, endDate : DateTime, periodStart : DateTime) : BigDecimal {
      return startDate.getBelgYearsBetween(endDate, periodStart)
    }

    /**
     * Only when the premium decreased, you can pass real eff/exp dates for rate eff/exp dates. This is applied to cancellation cost calculation
     * @param periodStart Start date of the policy. E.g. PolicyPeriod.PeriodStart
     * @param periodEnd End date of the policy. E.g. PolicyPeriod.PeriodEnd or PolicyPeriod.CancellationDate
     * @param rateEffectiveDate Virtual date for rating. E.g. CostData.RateEffectiveDate
     * @param rateExpirationDate Virtual date for rating. E.g. CostData.RateExpirationDate
     */
    function prorateBelgByMonths(periodStart : Date, periodEnd : Date,
                       rateEffectiveDate : Date, rateExpirationDate : Date,
                       amount : MonetaryAmount) : MonetaryAmount {
      return prorateBelgByMonths(periodStart, periodEnd, rateEffectiveDate, rateExpirationDate, amount.Amount).toEuros
    }

    function prorateBelgByQuarters(periodStart : Date, periodEnd : Date,
                                 rateEffectiveDate : Date, rateExpirationDate : Date,
                                 amount : MonetaryAmount) : MonetaryAmount {
      return prorateBelgByQuarters(periodStart, periodEnd, rateEffectiveDate, rateExpirationDate, amount.Amount).toEuros
    }

    function prorateBelgByHalfYears(periodStart : Date, periodEnd : Date,
                                   rateEffectiveDate : Date, rateExpirationDate : Date,
                                   amount : MonetaryAmount) : MonetaryAmount {
      return prorateBelgByHalfYears(periodStart, periodEnd, rateEffectiveDate, rateExpirationDate, amount.Amount).toEuros
    }

    function prorateBelgByYears(periodStart : Date, periodEnd : Date,
                                    rateEffectiveDate : Date, rateExpirationDate : Date,
                                    amount : MonetaryAmount) : MonetaryAmount {
      return prorateBelgByYears(periodStart, periodEnd, rateEffectiveDate, rateExpirationDate, amount.Amount).toEuros
    }

    /**
     * Only when the premium decreased, you can pass real eff/exp dates for rate eff/exp dates. This is applied to cancellation cost calculation
     * @param periodStart Start date of the policy. E.g. PolicyPeriod.PeriodStart
     * @param periodEnd End date of the policy. E.g. PolicyPeriod.PeriodEnd or PolicyPeriod.CancellationDate
     * @param rateEffectiveDate Virtual date for rating. E.g. CostData.RateEffectiveDate
     * @param rateExpirationDate Virtual date for rating. E.g. CostData.RateExpirationDate
     */
    function prorateBelgByMonths(periodStart : Date, periodEnd : Date,
                     rateEffectiveDate : Date, rateExpirationDate : Date,
                     amount : BigDecimal) : BigDecimal {
      return prorateByMonths(periodStart, periodEnd, rateEffectiveDate, rateExpirationDate, amount)
    }

    function prorateBelgByQuarters(periodStart : Date, periodEnd : Date,
                                 rateEffectiveDate : Date, rateExpirationDate : Date,
                                 amount : BigDecimal) : BigDecimal {
      return prorateByQuarters(periodStart, periodEnd, rateEffectiveDate, rateExpirationDate, amount)
    }

    function prorateBelgByHalfYears(periodStart : Date, periodEnd : Date,
                                   rateEffectiveDate : Date, rateExpirationDate : Date,
                                   amount : BigDecimal) : BigDecimal {
      return prorateByHalfYears(periodStart, periodEnd, rateEffectiveDate, rateExpirationDate, amount)
    }

    function prorateBelgByYears(periodStart : Date, periodEnd : Date,
                                    rateEffectiveDate : Date, rateExpirationDate : Date,
                                    amount : BigDecimal) : BigDecimal {
      return prorateByYears(periodStart, periodEnd, rateEffectiveDate, rateExpirationDate, amount)
    }

    /**
     * @param periodStart Start date of the policy. E.g. PolicyPeriod.PeriodStart
     * @param periodEnd End date of the policy. E.g. PolicyPeriod.PeriodEnd
     * @param rateEffectiveDate Virtual date for rating. E.g. CostData.RateEffectiveDate
     * @param rateExpirationDate Virtual date for rating. E.g. CostData.RateExpirationDate
     */
    protected function prorateByMonths(periodStart : Date, periodEnd : Date,
                             rateEffectiveDate : Date, rateExpirationDate : Date,
                             amount : BigDecimal) : BigDecimal {

      var months = financialMonthsBetween(rateEffectiveDate, rateExpirationDate, periodStart)
        return ((months / 12 * amount).setScale(2, HALF_UP)) // rating basis is 1 year, and there are 12 months in a year
    }


    protected function prorateByQuarters(periodStart : Date, periodEnd : Date,
                                         rateEffectiveDate : Date, rateExpirationDate : Date,
                                         amount : BigDecimal) : BigDecimal {
      var quarters = financialQuartersBetween(rateEffectiveDate, rateExpirationDate, periodStart)
      return ((quarters / 4 * amount).setScale(2, HALF_UP))
    }


    protected function prorateByHalfYears(periodStart : Date, periodEnd : Date,
                                         rateEffectiveDate : Date, rateExpirationDate : Date,
                                         amount : BigDecimal) : BigDecimal {
      var halfYears = financialHalfYearsBetween(rateEffectiveDate, rateExpirationDate, periodStart)
      return ((halfYears / 2 * amount).setScale(2, HALF_UP))
    }

    protected function prorateByYears(periodStart : Date, periodEnd : Date,
                                          rateEffectiveDate : Date, rateExpirationDate : Date,
                                          amount : BigDecimal) : BigDecimal {
      var years = financialYearsBetween(rateEffectiveDate, rateExpirationDate, periodStart)
      return ((years * amount).setScale(2, HALF_UP))
    }
    
    function calculateActualProration(amount : BigDecimal, termAmount : BigDecimal) : BigDecimal {
      return termAmount.IsZero ? BigDecimal.ZERO : (amount / termAmount).setScale(6, HALF_UP)
    }

    /**
     * This implementation is intentionally hardcoded to return zero, hence full reversals will always be created for cost changes.<br>
     * TransactionAdjustmentCalculator does not currently have the necessary information to correctly prorate Transactions attached to MonthlyProratableCosts
     */
    override function prorateFromStart(periodStart : Date, periodEnd : Date,
                                       prorateTo : Date, amount : BigDecimal) : BigDecimal {
      return BigDecimal.ZERO
    }
  }
}
