package gw.lob.pa.rating
uses java.util.Date
uses gw.rating.CostData
uses gw.financials.PolicyPeriodFXRateCache
uses belg.rating.BelgianProratableCostData

@Export
abstract class PACostData<R extends PACost> extends BelgianProratableCostData<R, PersonalAutoLine> {
  
  construct(effDate : Date, expDate : Date, c : Currency, rateCache : PolicyPeriodFXRateCache) {
    super(effDate, expDate, c, rateCache)
  }

  construct(effDate : Date, expDate : Date) {
    super(effDate, expDate)
  }

  construct(c : R) {
    super(c)
  }

  construct(c : R, installmentFreq : BillingPeriodicity) {
    super(c)
  }

  construct(c : R, rateCache : PolicyPeriodFXRateCache) {
    super(c, rateCache)
  }

  override function setSpecificFieldsOnCost(line : PersonalAutoLine, cost : R) {
    cost.setFieldValue("PersonalAutoLine", line.FixedId)
    setCostProration(line, cost)
  }

}
